<%
%>

    /**
     * Logout method
     *
     * @return \Bit\Network\Response
     */
    public function logout()
    {
        return $this->redirect($this->Auth->logout());
    }
