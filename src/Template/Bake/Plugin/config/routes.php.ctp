<%
%>
<?php
use Bit\Routing\RouteBuilder;
use Bit\Routing\Router;

Router::plugin(
    '<%= $plugin %>',
    ['path' => '/<%= $routePath %>'],
    function (RouteBuilder $routes) {
        $routes->fallbacks('DashedRoute');
    }
);
