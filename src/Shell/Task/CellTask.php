<?php
namespace Bake\Shell\Task;

/**
 * Task for creating cells.
 */
class CellTask extends SimpleBakeTask
{
    /**
     * Task name used in path generation.
     *
     * @var string
     */
    public $pathFragment = 'View/Cell/';

    /**
     * {@inheritDoc}
     */
    public function name()
    {
        return 'cell';
    }

    /**
     * {@inheritDoc}
     */
    public function fileName($name)
    {
        return $name . 'Cell.php';
    }

    /**
     * {@inheritDoc}
     */
    public function template()
    {
        return 'View/cell';
    }

    /**
     * Bake the Cell class and template file.
     *
     * @param string $name The name of the cell to make.
     * @return void
     */
    public function bake($name)
    {
        $this->bakeTemplate($name);
        return parent::bake($name);
    }

    /**
     * Bake an empty file for a cell.
     *
     * @param string $name The name of the cell a template is needed for.
     * @return void
     */
    public function bakeTemplate($name)
    {
        $templatePath = implode(DS, ['Template', 'Cell', $name, 'display.ctp']);
        $restore = $this->pathFragment;
        $this->pathFragment = $templatePath;

        $path = $this->getPath();
        $this->pathFragment = $restore;

        $this->createFile($path, '');
    }
}
