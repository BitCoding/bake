<?php
namespace Bake\Shell\Task;

/**
 * Shell Task code generator.
 */
class TaskTask extends SimpleBakeTask
{
    /**
     * Task name used in path generation.
     *
     * @var string
     */
    public $pathFragment = 'Shell/Task/';

    /**
     * {@inheritDoc}
     */
    public function name()
    {
        return 'task';
    }

    /**
     * {@inheritDoc}
     */
    public function fileName($name)
    {
        return $name . 'Task.php';
    }

    /**
     * {@inheritDoc}
     */
    public function template()
    {
        return 'Shell/task';
    }
}
