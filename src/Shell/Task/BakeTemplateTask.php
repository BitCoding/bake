<?php
namespace Bake\Shell\Task;

use Bake\View\BakeView;
use Bit\Console\Shell;
use Bit\Core\Configure;
use Bit\Core\Traits\Conventions;
use Bit\Event\Event;
use Bit\Event\EventManager;
use Bit\Network\Request;
use Bit\Network\Response;
use Bit\View\Exception\MissingTemplateException;
use Bit\View\ViewVarsTrait;

/**
 * Used by other tasks to generate templated output, Acts as an interface to BakeView
 */
class BakeTemplateTask extends Shell
{
    use Conventions;

    use ViewVarsTrait;

    /**
     * BakeView instance
     *
     * @var Bit\View\BakeView
     */
    public $View;

    /**
     * Get view instance
     *
     * @return \Bit\View\View
     * @triggers Bake.initialize $view
     */
    public function getView()
    {
        if ($this->View) {
            return $this->View;
        }

        $theme = isset($this->params['theme']) ? $this->params['theme'] : '';

        $viewOptions = [
            'helpers' => [
                'Bake.Bake',
                'Bake.DocBlock'
            ],
            'theme' => $theme
        ];
        $view = new BakeView(new Request(), new Response(), null, $viewOptions);
        $event = new Event('Bake.initialize', $view);
        EventManager::instance()->dispatch($event);
        $this->View = $event->subject;

        return $this->View;
    }

    /**
     * Runs the template
     *
     * @param string $template bake template to render
     * @param array|null $vars Additional vars to set to template scope.
     * @return string contents of generated code template
     */
    public function generate($template, $vars = null)
    {
        if ($vars !== null) {
            $this->set($vars);
        }

        $this->getView()->set($this->viewVars);

        try {
            return $this->View->render($template);
        } catch (MissingTemplateException $e) {
            $this->_io->verbose(sprintf('No bake template found for "%s"', $template));
            return '';
        }
    }
}
