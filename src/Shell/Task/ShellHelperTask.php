<?php
namespace Bake\Shell\Task;

/**
 * ShellHelper code generator.
 */
class ShellHelperTask extends SimpleBakeTask
{
    /**
     * Task name used in path generation.
     *
     * @var string
     */
    public $pathFragment = 'Shell/Helper/';

    /**
     * {@inheritDoc}
     */
    public function name()
    {
        return 'shell_helper';
    }

    /**
     * {@inheritDoc}
     */
    public function fileName($name)
    {
        return $name . 'Helper.php';
    }

    /**
     * {@inheritDoc}
     */
    public function template()
    {
        return 'Shell/helper';
    }
}
