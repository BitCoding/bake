<?php
namespace Bake\Shell\Task;

/**
 * ShellHelper code generator.
 */
class FormTask extends SimpleBakeTask
{
    /**
     * Task name used in path generation.
     *
     * @var string
     */
    public $pathFragment = 'Form/';

    /**
     * {@inheritDoc}
     */
    public function name()
    {
        return 'form';
    }

    /**
     * {@inheritDoc}
     */
    public function fileName($name)
    {
        return $name . 'Form.php';
    }

    /**
     * {@inheritDoc}
     */
    public function template()
    {
        return 'Form/form';
    }
}
