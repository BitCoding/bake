<?php
namespace Bake\Shell\Task;

/**
 * Helper code generator.
 */
class HelperTask extends SimpleBakeTask
{
    /**
     * Task name used in path generation.
     *
     * @var string
     */
    public $pathFragment = 'View/Helper/';

    /**
     * {@inheritDoc}
     */
    public function name()
    {
        return 'helper';
    }

    /**
     * {@inheritDoc}
     */
    public function fileName($name)
    {
        return $name . 'Helper.php';
    }

    /**
     * {@inheritDoc}
     */
    public function template()
    {
        return 'View/helper';
    }
}
